def romb(n):
    for i,y in zip(range(1,n+1), range (1,2*n,2)):
        print(' '*(n-i)+'*'*y)
    for i,y in zip(range(1,n), range (2*(n-1),1,-2)):
        print(' '*(i)+'*'*(y-1))

# if __name__=='__main__': STARE
#     romb(12) STARE

def lejek(n):
    for a, b in zip(range(n+1,n//2,-1), range(2*n,0,-2)):
        print(' '*(n-(a-1))+'*'*b)
    for a in range(0,n//2):
        print(' '*(n//2)+'*'*n+' '*(n//2))

def choinka(n):
    if n%2==0:
        n=n+1
        for a, b in zip(range(n,4,-1), range(0,2*n,2)):
            print(' '*a+'*'*(b+1))
        for a, b in zip(range(n,4,-1), range(n//2,2*n,2)):
            print(' '*(a-3)+'*'*(b+1))
        for a, b in zip(range(n,0,-1), range(n,2*n,2)):
            print(' '*(a-5)+'*'*(b+1))
        for a in range(0,n//4):
            print(' '*n+'*'*(n//4)+' '*2)
    else:
        for a, b in zip(range(n,4,-1), range(0,2*n,2)):
            print(' '*a+'*'*(b+1))
        for a, b in zip(range(n,4,-1), range(n//2,2*n,2)):
            print(' '*(a-3)+'*'*(b+1))
        for a, b in zip(range(n,0,-1), range(n,2*n,2)):
            print(' '*(a-5)+'*'*(b+1))
        for a in range(0,n//4):
            print(' '*n+'*'*(n//4)+' '*2)


if __name__=='__main__':
    choinka(10)
    print()
    print()
    print()
    romb(8)