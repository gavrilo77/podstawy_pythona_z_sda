class Tank:
    acceleration = 10
    ammo_usage = 1

    def __init__(self, army_name):
        self.army_name = army_name
        self.in_motion = False
        self.tower_rotate = False
        self.speed = 0
        self.ammo = 5

    def start_engine(self):
        self.in_motion = True

    def tower_move(self):
        self.tower_rotate = True

    def tower_stop(self):
        self.tower_rotate = False

    def accelerate(self, acceleration):
        if self.in_motion:
            self.speed += acceleration
        else:
            print('Your parameter in_motion is set to False')
            print()

    def shoot(self):
        if self.ammo > 0 and self.tower_rotate:
            self.ammo -= self.ammo_usage
            print(f'You have {self.ammo} shell(s) left')
            print()
        elif self.ammo > 0 and self.tower_rotate is False :
            print('Your tower is not rotating. Please start rotating it using <tower_move>')
            print()
        else:
            print('You are out of ammo')
            print()

    def stop_engine(self):
        self.in_motion = False
        self.tower_rotate = False
        self.speed = 0

    def print_info(self):
        print(f'Tank is part of {self.army_name}')
        print(f'Acceleration is {self.acceleration}')
        print(f'Speed is {self.speed}')
        if self.in_motion is True:
            print('Engine is on')
        else:
            print('Engine is off')
        if self.tower_rotate is True:
            print('Your tower is rotating')
        else:
            print('Your tower is not rotating')
        print(f'You have {self.ammo} shell(s) left')
        print()

abrams=Tank('US Army')
t90=Tank('Russian Army')
leopard=Tank('Bundeswehr')

leopard.start_engine()
leopard.accelerate(10)
leopard.tower_move()
leopard.shoot()
leopard.print_info()
leopard.tower_stop()
leopard.print_info()

