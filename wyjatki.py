# try:
#     print(10/0)
# except:
#     print('Nie dziel przez zero!!!')
# finally:
#     print('Blok finally. Zawsze wykonywany.')

# try:
#     raise SyntaxError
# except SyntaxError:
#     print('Nastapil blad syntaktyczny!')

class MojPierwszyWyjatek(Exception):
    pass

try:
    raise MojPierwszyWyjatek('My example message')
except MojPierwszyWyjatek as e:
    print(e, e.agrs, type(e))