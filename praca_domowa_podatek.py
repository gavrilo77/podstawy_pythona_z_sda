def oblicz_pensje_roczna(func):
    def wrapper(*args, **kwargs):
        # pensja_roczna = func() * 12
        print(f'Twoje roczne wynagrodzenie to:')
        func(*args, **kwargs)
    return wrapper

def pensja_miesieczna(number):
    number *= 12
    print(f'{number}')

@oblicz_pensje_roczna
def oblicz_podatek(number):
    number *= 12
    if number < 85000:
        print(f'Twoj podatek dochodowy to {number * 0.17}')
    else:
        podatek_pierwszy_prog = 85000 * 0.17
        podatek_drugi_prog = (number - 85000) * 0.32
        podatek_laczny = podatek_pierwszy_prog + podatek_drugi_prog
        print(f'Twoj podatek dochodowy to {podatek_laczny}')


if __name__ == '__main__':
    oblicz_pensje_roczna(pensja_miesieczna)(2000)
    oblicz_podatek(1000)