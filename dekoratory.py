# def my_decorator(func):
#     def wrapper(*args, **kwargs):
#         print('Hello, what is your name?')
#         func(*args, **kwargs)
#         print('Nice to meet you!')
#     return wrapper
#
# def name_func(name):
#     print(f'Hi my name is {name}.')
#
# @my_decorator
# def print_my_name_vol2(name):
#     print(f'Hi, my name is {name}')
#
# if __name__ == '__main__':
#     my_decorator(name_func)('Ala')
#     print()
#     print_my_name_vol2('Mieszko')

def my_number(func):
    def wrapper(*args, **kwargs):
        print('Your number is: ')
        func(*args, **kwargs)
    return wrapper

def number_is(number):
    print(f'{number}')

@my_number
def is_number_even(number):
    if number % 2:
        print(f'{number} is not even.')
    else:
        print(f'{number} is even')

if __name__ == '__main__':
    my_number(number_is)(13)
    is_number_even(11)
