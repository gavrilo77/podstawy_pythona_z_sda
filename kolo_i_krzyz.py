import random

plansza_gry = {'1': ' ' , '2': ' ' , '3': ' ' ,
            '4': ' ' , '5': ' ' , '6': ' ' ,
            '7': ' ' , '8': ' ' , '9': ' ' }

plansza_keys = []

for key in plansza_gry:
    plansza_keys.append(key)

def utworz_plansze(plansza):
    print(plansza['1'] + '|' + plansza['2'] + '|' + plansza['3'])
    print('-+-+-')
    print(plansza['4'] + '|' + plansza['5'] + '|' + plansza['6'])
    print('-+-+-')
    print(plansza['7'] + '|' + plansza['8'] + '|' + plansza['9'])


def kolko_i_krzyzyk():
    print('Plansza gry, którą widzisz poniżej, zbudowana jest podobnie jak klawiatura')
    print('dawnych telefonów komórkowych. Aby zapełnić odpowiednie pole <krzyżykiem>,')
    print('którym sterujesz, w odpowiedzi na pytanie zadane przez Grę podaj numer')
    print('odpowiedniego pola (od 1 do 9). Poniżej - dla opornych - ściąga pokazująca,')
    print('jak dokładnie wygląda plansza:')
    print('1' + '|' + '2' + '|' + '3')
    print('-+-+-')
    print('4' + '|' + '5' + '|' + '6')
    print('-+-+-')
    print('7' + '|' + '8' + '|' + '9')
    print()
    print()
    print()

    czyja_tura = 'X'
    liczba_ruchow = 0

    for i in range(10):
        utworz_plansze(plansza_gry)
        # print('To twój ruch, ' + czyja_tura + '. Które pole chcesz oznaczyć?')

        if czyja_tura == 'X':
            print('To twój ruch, ' + czyja_tura + '. Które pole chcesz oznaczyć?')
            ruch = input()
        if czyja_tura == 'O':
            print('Ruch komputera...')
            ruch = random.choice(['1', '2', '3', '4', '5', '6', '7', '8', '9'])

        if plansza_gry[ruch] == ' ':
            plansza_gry[ruch] = czyja_tura
            liczba_ruchow += 1
        else:
            print('To pole jest już zajęte. Wybierz inne pole.')
            continue

        if liczba_ruchow >= 5:
            if plansza_gry['1'] == plansza_gry['2'] == plansza_gry['3'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['4'] == plansza_gry['5'] == plansza_gry['6'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['7'] == plansza_gry['8'] == plansza_gry['9'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['1'] == plansza_gry['4'] == plansza_gry['7'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['2'] == plansza_gry['5'] == plansza_gry['8'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['3'] == plansza_gry['6'] == plansza_gry['9'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['1'] == plansza_gry['5'] == plansza_gry['9'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
            elif plansza_gry['7'] == plansza_gry['5'] == plansza_gry['3'] != ' ':
                utworz_plansze(plansza_gry)
                print('KONIEC GRY. WYGRAŁ ' + czyja_tura + '.')
                break
        if liczba_ruchow == 9:
            print('KONIEC GRY. REMIS!')

        if czyja_tura == 'X':
            czyja_tura = 'O'
        else:
            czyja_tura = 'X'

    restart = input("Czy chcesz zagrać ponownie? (y/n)")
    if restart == "y" or restart == "Y":
        for key in plansza_keys:
            plansza_gry[key] = ' '

        kolko_i_krzyzyk()

if __name__=='__main__':
    kolko_i_krzyzyk()
