class Pracownik:
    roczna_podwyzka = 1.04

    def __init__(self, imie, nazwisko, email, pensja):
        self.imie = imie
        self.nazwisko = nazwisko
        self.email = email
        self.pensja = float(pensja)

    def oblicz_pensje_po_podwyzcze(self):
        self.pensja *= self.roczna_podwyzka

    def oblicz_podatek_dochodowy(self):
        # 17% , 32% przy 85k
        self.roczna_pensja = self.pensja * 12

        if self.roczna_pensja < 85000:
            print(f'Twoj podatek dochodowy to {self.roczna_pensja*0.17}')
        else:
            podatek_pierwszy_prog = 85000 * 0.17
            podatek_drugi_prog = (self.roczna_pensja - 85000) * 0.32
            patek_laczny = podatek_pierwszy_prog + podatek_drugi_prog
            print(f'Twoj podatek dochodowy to {patek_laczny}')

    @classmethod
    def ustaw_roczna_podwyzke(cls, nowa_roczna_podwyzka):
        cls.roczna_podwyzka = nowa_roczna_podwyzka

    @staticmethod
    def wyswietl_informacje_podatkowe(kwota):
        print('Pierwszy prog podatkowy: 17%')
        print(f'Dochod powyzej {kwota} jest objety drugim progiem podatkowym')
        print('Drugi prog podatkowy: 32%')



class Developer(Pracownik):
    roczna_podwyzka = 1.10

    def __init__(self, imie, nazwisko, email, pensja, jezyk_programowania):
        super().__init__(imie, nazwisko, email, pensja)
        self.jezyk_programowania=jezyk_programowania

cezary_pra = Pracownik('Cezary', 'MojeNazwisko', 'sluzbowy@email', 10000)
cezary_dev = Developer('Cezary', 'MojeNazwisko', 'sluzbowy@email', 10000, 'python')

print(cezary_pra.roczna_podwyzka)
print(cezary_dev.roczna_podwyzka)
print(cezary_dev.imie)
print(cezary_dev.nazwisko)
print(cezary_dev.email)
print(cezary_dev.pensja)
print(cezary_dev.jezyk_programowania)
