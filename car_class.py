class Car:
    acceleration=10

    def __init__(self, registration_number):
        self.registration_number = registration_number
        self.in_motion=False
        self.speed=0

    def print_info(self):
        print(f'registration_number is {self.registration_number}')
        print(f'acceleration is {self.acceleration}')
        print(f'speed is {self.speed}')
        print(f'in_motion is {self.in_motion}')
        print()

    def drive(self):
        self.in_motion=True

    def accelerate(self, acceleration):
        if self.in_motion:
            self.speed += acceleration
        else:
            print('Your parameter in_motion is set to False')
            print()

    def stop(self):
        self.in_motion=False
        self.speed=0


audi=Car('w1')
volvo=Car('w2')
mercedes=Car('w3')

audi.accelerate(18)
audi.drive()
audi.accelerate(18)
audi.accelerate(1)
audi.accelerate(2)

audi.print_info()
